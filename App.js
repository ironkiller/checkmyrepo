/**
 *
 */
import React from 'react';
import AppContainer from './src/AppContainer';

/**
 *
 * @param {*} state
 * @param {*} action
 */

const App = () => {
  return <AppContainer />;
};
/**
 *
 */
export default App;
