/*
 * redux imports
 */
import {connect} from 'react-redux';
import {
  sendingUrl,
  resetNetworkField,
} from '../Redux/Repository/repositoryActions';
/*
 *  Screen imports
 */
import SendUrlScreen from '../Screen/SendUrlScreen';
/**
 * @param {*} state
 */
const mapStateToProps = state => {
  return {
    appState: state.repository,
  };
};
/**
 * @param {*} dispatch
 */
const mapDispatchToProps = dispatch => {
  return {
    sendingUrl: (user, repo) => dispatch(sendingUrl(user, repo)),
    resetNetworkField: () => dispatch(resetNetworkField()),
  };
};
/*
 *
 */
const SendUrlScreenContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(SendUrlScreen);
export default SendUrlScreenContainer;
