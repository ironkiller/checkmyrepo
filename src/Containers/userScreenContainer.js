/*
 * redux imports
 */
import {connect} from 'react-redux';
import {addUser} from '../Redux/Repository/repositoryActions';
/*
 *  Screen imports
 */
import UserScreen from '../Screen/UserScreen';
/**
 * @param {*} state
 */
const mapStateToProps = state => {
  console.log('mapStateToProps', state.repository);
  return {
    appState: state.repository,
  };
};
/**
 * @param {*} dispatch
 */
const mapDispatchToProps = dispatch => {
  return {
    addUser: username => dispatch(addUser(username)),
  };
};
/*
 *
 */
const UserScreenContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(UserScreen);
export default UserScreenContainer;
