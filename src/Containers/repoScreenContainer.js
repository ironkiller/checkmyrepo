/*
 * redux imports
 */
import {connect} from 'react-redux';
import {addRepo, resetIsValideUrl} from '../Redux/Repository/repositoryActions';
/*
 *  Screen imports
 */
import RepoScreen from '../Screen/RepoScreen';
/**
 *
 * @param {*} state
 */
const mapStateToProps = state => {
  console.log('mapStateToProps', state.repository);
  return {
    appState: state.repository,
  };
};
/**
 *
 * @param {*} dispatch
 */
const mapDispatchToProps = dispatch => {
  return {
    addRepo: repoName => dispatch(addRepo(repoName)),
    resetIsValideUrl: () => dispatch(resetIsValideUrl()),
  };
};
const RepoScreenContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(RepoScreen);
export default RepoScreenContainer;
