/*
 * redux imports
 */
import {connect} from 'react-redux';
import {
  urlCheck,
  resetIsValideUrl,
} from '../Redux/Repository/repositoryActions';
/*
 *  Screen imports
 */
import CheckFieldScreen from '../Screen/CheckFieldScreen';
/**
 *
 * @param {*} state
 */
const mapStateToProps = state => {
  console.log('mapStateToProps', state.repository);
  return {
    appState: state.repository,
  };
};
/**
 *
 * @param {*} dispatch
 */
const mapDispatchToProps = dispatch => {
  return {
    urlCheck: (user, repo) => dispatch(urlCheck(user, repo)),
    resetIsValideUrl: () => dispatch(resetIsValideUrl()),
  };
};
const CheckFieldScreenContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(CheckFieldScreen);
export default CheckFieldScreenContainer;
