/*
 * redux imports
 */
import {connect} from 'react-redux';
/*
 *  Screen imports
 */
import HomeScreen from '../Screen/HomeScreen';
/**
 * @param {*} state
 */
const mapStateToProps = state => {
  console.log('mapStateToProps', state.repository);
  return {
    appState: state.repository,
  };
};
/*
 *
 */
const HomeScreenContainer = connect(
  mapStateToProps,
  null,
)(HomeScreen);
export default HomeScreenContainer;
