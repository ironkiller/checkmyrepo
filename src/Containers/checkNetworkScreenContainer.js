/*
 * redux imports
 */
import {connect} from 'react-redux';
import {
  checkNetwork,
  resetIsValideUrl,
} from '../Redux/Repository/repositoryActions';
/*
 *  Screen imports
 */
import CheckNetworkScreen from '../Screen/CheckNetworkScreen';
/**
 *
 * @param {*} state
 */
const mapStateToProps = state => {
  return {
    appState: state.repository,
  };
};
/**
 *
 * @param {*} dispatch
 */
const mapDispatchToProps = dispatch => {
  return {
    checkNetwork: () => dispatch(checkNetwork()),
    resetIsValideUrl: () => dispatch(resetIsValideUrl()),
  };
};
/*
 *
 */
const CheckNetworkScreenContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(CheckNetworkScreen);
export default CheckNetworkScreenContainer;
