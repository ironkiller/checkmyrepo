import {combineReducers} from 'redux';
import repositoryReducer from './Repository/repositoryReducer';
/*
 *
 */
const rootReducer = combineReducers({
  repository: repositoryReducer,
});
/*
 *
 */
export default rootReducer;
