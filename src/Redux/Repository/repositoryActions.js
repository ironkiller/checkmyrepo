import {
  ADD_REPO,
  ADD_USER,
  AWAIT_ACTION_URL,
  FETCH_RESPONSE_URL,
  FETCH_FAILURE_URL,
  RESET_ISVALIDE_URL,
  FETCH_NETWORK_STATE,
  FETCH_RESPONSE_SEND,
  FETCH_FAILURE_SENDING,
  RESET_NETWORK_FIELD,
} from './repositoryTypes';
/*
 *
 */
import NetInfo from '@react-native-community/netinfo';
/*
 *
 * @param {*} user
 * @param {*} repo
 */
export const urlCheck = (user, repo) => {
  console.log(`Actions - urlCheck`);
  const url = `https://gitlab.com/${user}/${repo}`;
  return dispatch => {
    dispatch(awaitActionUrl());
    fetch(url, {method: 'get'})
      .then(resp => {
        console.log(
          `Actions - urlCheck-then  response: ${JSON.stringify(resp)}`,
        );
        resp = resp.url === url ? 'ok' : 'error';
        dispatch(fetchResponseSuccess(resp));
      })
      .catch(err => {
        console.log(`Actions - urlCheck-catch ${err}`);
        dispatch(fetchFailureUrl(err));
      });
  };
};
/**
 * @param {*} user
 * @param {*} repo
 */
export const sendingUrl = (user, repo) => {
  const myUrl = 'https://fakeUrl.xxx';
  const url = myUrl;
  const repoUrl = `https://gitlab.com/${user}/${repo}`;
  return dispatch => {
    dispatch(awaitActionUrl());
    console.log(`action - sendingUrl  repoUrl: ${repoUrl}, sender: ${user}`);
    fetch(url, {
      method: 'post',
      body: `repoUrl: ${repoUrl}, sender: ${user}`,
      headers: {
        'Content-Type': 'text/plain',
      },
    })
      .then(resp => {
        console.log(`action - sendingUrl  resp: ${JSON.stringify(resp)}`);
        dispatch(fetchSuccessSendUrl(resp.status));
      })
      .catch(err => {
        console.log(`action - sendingUrl-catch  resp: ${err}`);
        dispatch(fetchFailureSending(err));
      });
  };
};
/*
 *
 */
export const checkNetwork = () => {
  return dispatch => {
    dispatch(awaitActionUrl());
    NetInfo.fetch()
      .then(state => {
        console.log('Connection type', state.type);
        console.log('Is connected?', state.isConnected);
        const networkCheck = state.isConnected ? 'checked' : 'error';
        dispatch(fetchNetworkState(networkCheck));
      })
      .catch(err => {
        dispatch(fetchFailureUrl(err.message));
      });
  };
};
/**
 *
 * @param {*} user
 */
export const addUser = user => {
  return {
    type: ADD_USER,
    payload: user,
  };
};
/*
 * @param {*} repo
 */
export const addRepo = repo => {
  return {
    type: ADD_REPO,
    payload: repo,
  };
};
/*
 *
 */
export const awaitActionUrl = () => {
  return {
    type: AWAIT_ACTION_URL,
  };
};
/**
 *
 */
export const fetchFailureUrl = err => {
  return {
    type: FETCH_FAILURE_URL,
    payload: err,
  };
};
export const fetchResponseSuccess = url => {
  return {
    type: FETCH_RESPONSE_URL,
    payload: url,
  };
};
/*
 *
 */
export const resetIsValideUrl = () => {
  return {
    type: RESET_ISVALIDE_URL,
    payload: '',
  };
};
/**
 *
 */
export const fetchNetworkState = stateNetwork => {
  return {
    type: FETCH_NETWORK_STATE,
    payload: stateNetwork,
  };
};
/**
 *
 */
export const fetchSuccessSendUrl = statusCode => {
  return {
    type: FETCH_RESPONSE_SEND,
    payload: statusCode,
  };
};
/*
 *
 */
export const fetchFailureSending = err => {
  return {
    type: FETCH_FAILURE_SENDING,
    payload: err,
  };
};
/*
 *
 */
export const resetNetworkField = () => {
  return {
    type: RESET_NETWORK_FIELD,
    payload: 'disable',
  };
};
