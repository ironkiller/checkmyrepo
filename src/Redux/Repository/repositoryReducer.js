import {
  ADD_REPO,
  ADD_USER,
  AWAIT_ACTION_URL,
  FETCH_RESPONSE_URL,
  FETCH_FAILURE_URL,
  RESET_ISVALIDE_URL,
  FETCH_NETWORK_STATE,
  FETCH_RESPONSE_SEND,
  FETCH_FAILURE_SENDING,
  RESET_NETWORK_FIELD,
} from './repositoryTypes';
/*
 *
 */
const initialState = {
  user: '',
  repo: '',
  isLoading: false,
  error: null,
  isValidUrl: '',
  networkCheck: 'disable',
  statusCode: 'none',
};
/**
 *
 * @param {*} state
 * @param {*} action
 */
const repositoryReducer = (state = initialState, action) => {
  switch (action.type) {
    /**
     *
     */
    case ADD_USER:
      console.log(
        `reducer actionType: ${action.type} payload:${action.payload}`,
      );
      return {
        ...state,
        user: action.payload,
      };
    /**
     *
     */ case FETCH_NETWORK_STATE:
      console.log(`Reducer azione: ${action.type}  payload: ${action.payload}`);
      return {
        ...state,
        isLoading: false,
        networkCheck: action.payload,
      };
    case ADD_REPO:
      return {
        ...state,
        repo: action.payload,
      };
    /*
     *
     */
    case AWAIT_ACTION_URL:
      return {
        ...state,
        isLoading: true,
      };
    /**
     *
     */
    case FETCH_RESPONSE_URL:
      return {
        ...state,
        isLoading: false,
        isValidUrl: action.payload,
        error: null,
      };
    /**
     *
     */
    case FETCH_FAILURE_SENDING:
      return {
        ...state,
        isLoading: false,
        statusCode: '',
        error: action.payload,
      };
    /**
     *
     */
    case FETCH_NETWORK_STATE:
      console.log(`Reducer azione: ${action.type}  payload: ${action.payload}`);
      return {
        ...state,
        isLoading: false,
        networkCheck: action.payload,
      };
    /**
     *
     */
    case FETCH_RESPONSE_SEND:
      console.log(`Reducer azione: ${action.type}  payload: ${action.payload}`);
      return {
        ...state,
        isLoading: false,
        error: null,
        statusCode: action.payload,
      };
    /*
     *
     */
    case FETCH_FAILURE_URL:
      return {
        ...state,
        isLoading: false,
        error: action.payload,
      };
    /*
     *
     */
    case RESET_ISVALIDE_URL:
      return {
        ...state,
        isValidUrl: action.payload,
      };
    /*
     *
     */
    case RESET_NETWORK_FIELD:
      return {
        ...state,
        networkCheck: action.payload,
      };
    /**
     *
     */
    default:
      return state;
  }
};
/*
 *
 */
export default repositoryReducer;
