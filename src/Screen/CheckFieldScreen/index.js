import React, {useEffect} from 'react';
import InfoRepository from '../../Components/InfoRepository';
/*
 * @param {*} props
 */
const CheckFieldScreen = props => {
  const {user, repo, isValidUrl} = props.appState;
  const {navigation} = props;
  /*
   *
   */
  const actionBtn = () => {
    props.urlCheck(user, repo);
  };
  /**
   *
   */
  const handleBackgroundColor = () => {
    if (isValidUrl === '' || isValidUrl === 'ok') {
      return 'white';
    } else if (isValidUrl === 'error') {
      return 'lightsalmon';
    }
  };
  /*
   *
   */
  useEffect(() => {
    console.log('CheckFieldScreen - useEffect');
    if (isValidUrl === 'ok') {
      navigation.navigate('CheckNetworkScreen');
    }
  }, [isValidUrl, navigation]);
  /*
   *
   */
  return (
    <InfoRepository
      infoColor={handleBackgroundColor()}
      infoView={true}
      mode={'fields'}
      repositoryField={{user: user, repo: repo}}
      buttonLabel={'CHECK'}
      action={actionBtn}
      {...props}
    />
  );
};
/*
 *
 */
CheckFieldScreen.navigationOptions = ({navigation}) => {
  return {
    headerShown: false,
  };
};
/**
 *
 */
export default CheckFieldScreen;
