import React, {useEffect} from 'react';
import InfoRepository from '../../Components/InfoRepository';
/*
 * @param {*} props
 */
const CheckNetworkScreen = props => {
  const {user, repo, networkCheck, isLoading} = props.appState;
  console.log(
    `CheckNetworkScreen networkCheck: ${networkCheck} isLoading: ${isLoading}`,
  );
  const {navigation, resetIsValideUrl} = props;
  /*
   *
   */

  const actionBtn = () => {
    props.checkNetwork();
  };
  /*
   *
   */
  const handlerBackgroundColor = () => {
    if (networkCheck === 'disable' || networkCheck === 'checked') {
      return 'white';
    } else if (networkCheck === 'error') {
      return 'lightsalmon';
    }
  };
  /*
   *
   */
  useEffect(() => {
    console.log('CheckNetworkScreen - useEffect');
    if (networkCheck === 'checked') {
      navigation.navigate('SendUrlScreen');
    }
    resetIsValideUrl();
  }, [networkCheck, navigation, resetIsValideUrl]);
  /*
   *
   */
  return (
    <InfoRepository
      infoColor={handlerBackgroundColor()}
      infoView={true}
      mode={'internet'}
      repositoryField={{user: user, repo: repo}}
      buttonLabel={'CHECK'}
      action={actionBtn}
      {...props}
    />
  );
};
/*
 *
 */
CheckNetworkScreen.navigationOptions = ({navigation}) => {
  return {
    headerShown: false,
  };
};
/*
 *
 */
export default CheckNetworkScreen;
