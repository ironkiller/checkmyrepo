import React, {useEffect} from 'react';
import InfoRepository from '../../Components/InfoRepository';
/*
 * @param {*} props
 */
const SendUrlScreen = props => {
  const {user, repo, isLoading, statusCode, error} = props.appState;
  console.log(
    `SendUrlScreen -  status: ${statusCode},  isLoading:   ${isLoading}, error: ${error}`,
  );
  const {navigation, resetNetworkField} = props;
  /*
   *
   */
  const actionBtn = () => {
    props.sendingUrl(user, repo);
  };
  /*
   *
   */
  const handlerBackgroundColor = () => {
    if (
      (statusCode !== 200 && statusCode !== 'none') ||
      error === 'Network request failed'
    ) {
      return 'lightsalmon';
    } else {
      return 'lightgreen';
    }
  };
  /**
   *
   */
  useEffect(() => {
    console.log('SendUrlScreen - useEffect');
    if (statusCode === 200) {
      navigation.navigate('CoolScreen');
    }
    resetNetworkField();
  }, [statusCode, navigation, resetNetworkField]);
  /*
   *
   */
  return (
    <InfoRepository
      infoColor={handlerBackgroundColor()}
      infoView={false}
      repositoryField={{user: user, repo: repo}}
      buttonLabel={'SEND'}
      action={actionBtn}
      {...props}
    />
  );
};
/*
 *
 */
SendUrlScreen.navigationOptions = ({navigation}) => {
  return {
    headerShown: false,
  };
};
export default SendUrlScreen;
