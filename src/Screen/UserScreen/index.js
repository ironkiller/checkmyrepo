/*
 *
 */
import React from 'react';
import {ImageBackground, TouchableOpacity} from 'react-native';
import GenericForm from '../../Components/GenericForm';
/**
 *
 * @param {*} props
 */
const UserScreen = props => {
  /*
   *
   */
  return (
    <GenericForm
      mode={'userScreen'}
      placeholder={'Type your gitlab username'}
      {...props}
    />
  );
};
/*
 *
 */
UserScreen.navigationOptions = ({navigation}) => {
  return {
    title: 'USER',
    headerLeft: () => (
      <TouchableOpacity onPress={() => navigation.navigate('Home')}>
        <ImageBackground
          // eslint-disable-next-line react-native/no-inline-styles
          style={{height: 15, width: 20, marginLeft: 10}}
          source={require('../../Icons/back.png')}
        />
      </TouchableOpacity>
    ),
  };
};
/*
 *
 */
export default UserScreen;
