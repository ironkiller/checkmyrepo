const styles = {
  mainContainer: {flex: 1, flexDirection: 'column'},
  header: {flex: 1},
  footer: {
    flex: 1,
    paddingRight: '8%',
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  textFooter: {fontFamily: 'OpenSans-Bold', fontSize: 22},
  body: {
    flex: 6,
    flexDirection: 'column',
    alignItems: 'center',
    paddingTop: '10%',
  },
  textSetting: {fontSize: 35, fontFamily: 'OpenSans-Bold'},
};
export default styles;
