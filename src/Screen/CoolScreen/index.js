import React from 'react';
import {Text, View} from 'react-native';
import styles from './styles';
/*
 * @param {*} props
 */
const CoolScreen = props => {
  console.log('CoolScreen function');
  /*
   *
   */
  return (
    <View style={styles.mainContainer}>
      <View style={styles.header} />
      <View style={styles.body}>
        <Text style={styles.textSetting}>All done!</Text>
        <Text style={styles.textSetting}>Repository Sent.</Text>
      </View>
      <View style={styles.footer}>
        <Text style={styles.textFooter}>COOL</Text>
      </View>
    </View>
  );
};
/*
 *
 */
CoolScreen.navigationOptions = ({navigation}) => {
  return {
    headerShown: false,
  };
};
/*
 *
 */
export default CoolScreen;
