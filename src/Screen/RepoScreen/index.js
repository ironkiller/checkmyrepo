/*
 *
 */
import React from 'react';
import {ImageBackground, TouchableOpacity} from 'react-native';
import GenericForm from '../../Components/GenericForm';
/**
 *
 * @param {*} props
 */
const RepoScreen = props => {
  /*
   *
   */
  return (
    <GenericForm
      mode={'repoScreen'}
      placeholder={'Type your repository name'}
      {...props}
    />
  );
};
/*
 *
 */
RepoScreen.navigationOptions = ({navigation}) => {
  return {
    title: 'REPOSITORY',
    headerLeft: () => (
      <TouchableOpacity onPress={() => navigation.navigate('UserScreen')}>
        <ImageBackground
          // eslint-disable-next-line react-native/no-inline-styles
          style={{height: 15, width: 20, marginLeft: 10}}
          source={require('../../Icons/back.png')}
        />
      </TouchableOpacity>
    ),
  };
};
/*
 *
 */
export default RepoScreen;
