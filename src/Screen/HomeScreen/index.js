import React from 'react';
import InfoRepository from '../../Components/InfoRepository';
/*
 *
 * @param {*} props
 */
const HomeScreen = props => {
  /*
   *
   */
  const actionBtn = () => {
    props.navigation.navigate('UserScreen');
  };
  /*
   *
   */
  return (
    <InfoRepository
      infoView={false}
      mode={'internet'}
      // repositoryField={fakefield}
      buttonLabel={'CHECK'}
      action={actionBtn}
      {...props}
    />
  );
};
/*
 *
 */
HomeScreen.navigationOptions = ({navigation}) => {
  return {
    headerShown: false,
  };
};
/**
 *
 */
export default HomeScreen;
