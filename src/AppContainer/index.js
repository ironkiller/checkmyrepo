import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import UserScreenContainer from '../Containers/userScreenContainer';
import RepoScreenContainer from '../Containers/repoScreenContainer';
import CheckFieldScreenContainer from '../Containers/checkFieldScreenContainer';
import HomeScreenContainer from '../Containers/homeScreenContainer';
import CheckNetworkScreenContainer from '../Containers/checkNetworkScreenContainer';
import SendUrlScreenContainer from '../Containers/sendUrlScreenContainer';
import CoolScreen from '../Screen/CoolScreen';
/*
 *
 */
const AppNavigator = createStackNavigator(
  {
    Home: {screen: HomeScreenContainer},
    UserScreen: {screen: UserScreenContainer},
    RepoScreen: {screen: RepoScreenContainer},
    CheckScreen: {screen: CheckFieldScreenContainer},
    CheckNetworkScreen: {screen: CheckNetworkScreenContainer},
    SendUrlScreen: {screen: SendUrlScreenContainer},
    CoolScreen: {screen: CoolScreen},
  },
  {
    initialRouteName: 'Home',
  },
);
/*
 *
 */
const AppContainer = createAppContainer(AppNavigator);
export default AppContainer;
