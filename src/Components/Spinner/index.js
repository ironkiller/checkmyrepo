/**
 * React import
 */
import React from 'react';
/*
 * react native import
 */
import {View} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
/*
 *  local Layout import
 */
import layouts from './layouts';
/*
 * local styles import
 */
import styles from './styles';
/*
 *
 */
const MySpinner = props => {
  return (
    <View style={[styles.container, layouts.container]}>
      <Spinner
        visible={props.visible}
        textContent={'Loading...'}
        textStyle={styles.textStyle}
        size={layouts.size}
      />
    </View>
  );
};
/*
 *
 */
export default MySpinner;
