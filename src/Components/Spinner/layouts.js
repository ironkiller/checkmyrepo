const layouts = {
  container: {
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  size: 'large',
};
export default layouts;
