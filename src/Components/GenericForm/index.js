import React, {useState} from 'react';
import {View, Text, TouchableOpacity, TextInput} from 'react-native';
import styles from './styles';
/**
 *
 * @param {*} props
 */
const GenericForm = props => {
  console.log('GenericForm', props.appState);
  /*
   *
   */
  const [fieldName, setFieldName] = useState('');
  const {
    placeholder,
    mode,
    navigation,
    addUser,
    addRepo,
    resetIsValideUrl,
  } = props;
  /*
   *
   */
  const handlerFieldName = evt => {
    setFieldName(evt.nativeEvent.text);
  };
  /*
   *
   */
  const submit = () => {
    if (mode === 'userScreen') {
      addUser(fieldName);
      navigation.navigate('RepoScreen');
    } else if (mode === 'repoScreen') {
      addRepo(fieldName);
      resetIsValideUrl();
      navigation.navigate('CheckScreen');
    }
  };
  /**
   *
   */
  return (
    <View style={styles.mainContainer}>
      <View style={styles.textInputView}>
        <TextInput
          onChange={handlerFieldName}
          placeholder={placeholder}
          style={styles.textInput}
          underlineColorAndroid={'black'}
          value={fieldName}
        />
      </View>
      <View style={styles.body} />
      <View style={styles.touchableView}>
        <TouchableOpacity
          disabled={fieldName.length > 0 ? false : true}
          onPress={submit}
          style={styles.touchableBtn}>
          <Text
            style={[
              styles.touchableBtnText,
              // eslint-disable-next-line react-native/no-inline-styles
              {color: fieldName.length > 0 ? null : 'red'},
            ]}>
            DONE
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
/*
 *
 */
export default GenericForm;
