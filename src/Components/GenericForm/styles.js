const styles = {
  mainContainer: {
    flex: 1,
    flexDirection: 'column',
  },
  textInputView: {
    flex: 1,

    justifyContent: 'flex-end',
    paddingLeft: '5%',
    paddingRight: '5%',
    marginTop: 5,
  },
  body: {flex: 6},
  textInput: {
    fontSize: 22,
  },
  touchableView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  touchableBtn: {marginRight: '4%'},
  touchableBtnText: {
    fontSize: 20,
    fontFamily: 'OpenSans-Bold',
  },
};
export default styles;
