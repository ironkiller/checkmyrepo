const styles = {
  mainContainer: {
    flex: 1,
    flexDirection: 'column',
  },
  header: {
    flex: 1,
    justifyContent: 'center',
    paddingLeft: '5%',
  },
  textHeader: {fontSize: 22, fontFamily: 'OpenSans-Bold'},
  body: {flex: 6},
  fieldRepo: {
    flex: 1,
    flexDirection: 'column',
    paddingLeft: '5%',
  },
  rowField: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  info: {flex: 2, flexDirection: 'column'},
  innerInfoView: {flexDirection: 'row', paddingLeft: '5%'},
  textInfo: {fontSize: 18, fontFamily: 'OpenSans-Bold'},
  textInfoBold: {
    fontSize: 18,
    fontFamily: 'OpenSans-Bold',
    marginLeft: 5,
  },
  footer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  textRow: {fontSize: 25, fontFamily: 'OpenSans-Bold', color: 'gray'},
  slash: {fontSize: 26, fontFamily: 'OpenSans-Bold'},
  touchableFooter: {marginRight: '4%'},
  touchableBtnText: {
    fontSize: 20,
    fontFamily: 'OpenSans-Bold',
  },
};

export default styles;
