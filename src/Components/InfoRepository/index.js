import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import styles from './styles';
import MySpinner from '../Spinner';
/**
 *
 * @param {*} props
 */
const InfoRepository = props => {
  const user = props?.repositoryField?.user;
  const repo = props?.repositoryField?.repo;
  const {infoView, mode, buttonLabel, action, infoColor} = props;
  const isloading = props?.appState?.isLoading;
  /*
   *
   */
  return (
    <View style={[styles.mainContainer, {backgroundColor: infoColor}]}>
      <MySpinner visible={isloading} />
      <View style={styles.header}>
        <Text style={styles.textHeader}>Set the repository address</Text>
      </View>
      <View style={styles.body}>
        <View style={styles.fieldRepo}>
          <View style={styles.rowField}>
            {/*eslint-disable-next-line react-native/no-inline-styles*/}
            <Text style={[styles.textRow, {color: 'black'}]}>gitHub.com</Text>
          </View>
          <View style={styles.rowField}>
            <Text style={styles.slash}>/</Text>
            <Text style={styles.textRow}>{user || 'user'}</Text>
          </View>
          <View style={styles.rowField}>
            <Text style={styles.slash}>/</Text>
            <Text style={styles.textRow}>{repo || 'repo'}</Text>
          </View>
        </View>

        <View style={styles.info}>
          {infoView ? (
            mode && mode === 'fields' ? (
              <>
                <View style={styles.innerInfoView}>
                  <Text style={styles.textInfo}>Check your</Text>
                  <Text style={styles.textInfoBold}>username</Text>
                </View>
                <View style={styles.innerInfoView}>
                  <Text style={styles.textInfo}>or your</Text>
                  <Text style={styles.textInfoBold}>repository</Text>
                  <Text style={styles.textInfo}> name</Text>
                </View>
              </>
            ) : (
              <View style={styles.innerInfoView}>
                <Text style={styles.textInfo}>Check your</Text>
                <Text style={styles.textInfoBold}>internet connection</Text>
              </View>
            )
          ) : null}
        </View>
      </View>
      <View style={styles.footer}>
        <TouchableOpacity style={styles.touchableFooter} onPress={action}>
          <Text style={styles.touchableBtnText}>{buttonLabel}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
/*
 *
 */
export default InfoRepository;
